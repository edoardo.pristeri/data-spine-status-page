from flask import Flask, render_template, request
from flask_oidc import OpenIDConnect
from werkzeug.routing import Rule
import httpx
import logging
from sqlitedict import SqliteDict

import argparse

logging.basicConfig(level=logging.DEBUG)

parser = argparse.ArgumentParser()
parser.add_argument("--mount_point", help="Mount point for the SRT webapp instance", type=str)
args = parser.parse_args()

app = Flask(__name__, static_url_path=f"/{args.mount_point}")
app.url_rule_class = lambda path, **options: Rule(f"/{args.mount_point}" + path, **options)
app.config.update({
    'SECRET_KEY': 'SomethingNotEntirelySecret',
    'TESTING': True,
    'DEBUG': True,
    'OIDC_CLIENT_SECRETS': 'data/client_secrets.json',
    'OIDC_ID_TOKEN_COOKIE_SECURE': False,
    'OIDC_REQUIRE_VERIFIED_EMAIL': False,
    'OIDC_USER_INFO_ENABLED': True,
    'OIDC_OPENID_REALM': 'efpf',
    'OIDC_SCOPES': ['openid', 'email', 'profile'],
    'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post',
    'OIDC_CALLBACK_ROUTE': '/custom_callback'
    #'OVERWRITE_REDIRECT_URI': 'https://efpf.polito.it/s/oidc_callback'
})

oidc = OpenIDConnect(app, credentials_store=SqliteDict('data/credentials.db', autocommit=True))

@app.route('/custom_callback')
@oidc.custom_callback
def callback(data):
    print(data)
    return 'Hello. You submitted %s' % data

@app.route('/')
@oidc.require_login
def index():
    return render_template("index.html", token=oidc.get_access_token())