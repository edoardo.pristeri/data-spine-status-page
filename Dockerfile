FROM python:3.9-buster
WORKDIR /app
COPY ./requirements.txt /app
RUN pip install --upgrade pip && pip install -r requirements.txt
COPY ./ /app
ENTRYPOINT python -m src.wsgi --mount_point=$MOUNT_POINT
